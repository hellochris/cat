### 基于Spring Cloud Finchley.SR1, Spring Boot 2.0.4.RELEASE的微服务实践案例


### 在线体验
[Spring Eureka URL](http://120.78.59.68:8706)

###版本
- 基于最新的Spring Boot 2.0.4.RELEASE (2018-08)
- 基于最新的Spring Framework 5.0.8.RELEASE (2018-08)
- 基于最新的Spring Cloud Finchley.SR1 (2018-08)
- Java 8
 
[源码获取](https://gitee.com/hellochris/cat.git)   


### 前端解决方案
https://gitee.com/smallweigit/avue


###功能需求清单（并不是指已开发完成）
- 完善登录：账号密码模式、短信验证码模式、社交账号模式均整合Spring security oAuth
- 单点登录：基于Srping security oAuth 提供单点登录接口，方便其他系统对接
- 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 机构管理：配置系统组织机构（公司、部门、小组），树结构展现，可随意调整上下级。
- 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
- 动态路由：基于zuul实现动态路由，后端可配置化
- 终端管理：动态配置oauth终端，后端可配置化
- 字典管理：对系统中经常使用的一些较为固定的数据进行维护，如：是否、男女、类别、级别等。
- 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
- 服务限流：多种维度的流量控制（服务、IP、用户等）
- 消息总线：配置动态实时刷新
- 分库分表：shardingdbc分库分表策略
- 数据权限: 使用mybatis对原查询做增强，业务代码不用控制，即可实现。
- 文件系统: 支持FastDFS、七牛云，扩展API几行代码实现上传下载
- 消息中心：短信、邮件模板发送，几行代码实现发送
- 聚合文档：基于zuul实现 swagger各个模块的实现
- 代码生成：前后端代码的生成，支持Vue
- 缓存管理：基于Cache Cloud 保证Redis 的高可用
- 服务监控: Spring Boot Admin2
- 分布式任务调度： 基于elastic-job的分布式文件系统，zookeeper做调度中心
- zipkin链路追踪： 数据保存ELK，图形化展示
- pinpoint链路追踪： 数据保存hbase，图形化展示


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

### 捐赠 | Donate
![感谢支持](https://images.gitee.com/uploads/images/2018/0725/233850_5ec9cf46_1609871.png "我的微信支付宝二合一收款码")


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. **MyBatis-Plus**码云封面人物（含该作品作者）风采展示 [https://gitee.com/gitee-stars/11](https://gitee.com/gitee-stars/11)




