/**
 * filecomment: 
 * filename: Application.java
 * author: shangpan
 * date: Aug 15, 2018
 */
package com.wu.cat.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author shangpan
 * @since Aug 15, 2018
 */

@RestController
@EnableEurekaServer
@SpringBootApplication
public class EurekaApplication {

	@Configuration
	public static class SecurityPermitAllConfig extends WebSecurityConfigurerAdapter {
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests().anyRequest().permitAll()
					.and().csrf().disable();
		}
	}

    @GetMapping("/index")
    public String index() {
        return "Spring Cloud, EnableEurekaServer!!!";
    }



	public static void main(String[] args) {
		SpringApplication.run(EurekaApplication.class, args);
	}
}
